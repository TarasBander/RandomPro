#include <cstdlib>
#include <iostream>
#include <ctime>
using namespace std;

int main(int argc, char *argv[])
{

    if (argc > 1)
    {
        cout << "hello " << argv[1] << endl;
    }

    srand(time(0));
    int random_variable = rand()%100;
    int count = 0;
    int value;
    for (;;)
    {
        cout << "Please enter value : " << endl;
        cin >> value;
        count++;
        if (value < random_variable)
            cout << "your value is less than random variable " << endl;
        else if (value > random_variable)
            cout << "your value is greater than random variable " << endl;
        else
        {
            cout << "your value is equel to random variable " << endl;
            break;
        }
    }

    cout << "you done " << count << " iteration." << endl;

    return 0;
}
